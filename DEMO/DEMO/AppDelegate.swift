//
//  AppDelegate.swift
//  SharingPushNotification
//
//  Created by Adinan Yujaroen on 25/2/2563 BE.
//  Copyright © 2563 Adinan Yujaroen. All rights reserved.
//

import UIKit
import UserNotifications
import SafariServices

enum Identifiers {
  static let viewAction = "VIEW_IDENTIFIER"
  static let newsCategory = "NEWS_CATEGORY"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    // Check if launched from notification
    UNUserNotificationCenter.current().delegate = self
    registerForPushNotifications()
    let notificationOption = launchOptions?[.remoteNotification]

    // 1
    if let notification = notificationOption as? [String: AnyObject],
      let aps = notification["aps"] as? [String: AnyObject] {
      // this for get payload data from notification
      print(aps)
    }
    
    return true
  }

  // MARK: UISceneSession Lifecycle

  func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }

  func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
  }

  func registerForPushNotifications() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { isSuccess, error in
       print("Permission isSuccess? : \(isSuccess)") // 3
      guard isSuccess else { return }
      
      //set action follow category
      let viewAction = UNNotificationAction(identifier: Identifiers.viewAction, title: "NEWS daily", options: .foreground)
      let newsCategory = UNNotificationCategory(identifier: Identifiers.newsCategory, actions: [viewAction], intentIdentifiers: [], options: [])
      UNUserNotificationCenter.current().setNotificationCategories([newsCategory])
    }
  }
  
  //use in real iPhone
  func getNotificationSettings() {
    UNUserNotificationCenter.current().getNotificationSettings { settings in
      if settings.authorizationStatus == .authorized {
        // this code for Registering With APNs
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
      } else if settings.authorizationStatus == .denied {
        print("denied")
      }
    }
  }
  
//  this will call if regis success
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    print("device Token = \(deviceToken.base64EncodedString())")
    let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    let token = tokenParts.joined()
    print("Device Token: \(token)")
  }
  
//  this will call if regis fail
  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    print(error)
  }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    print("After click notification")
    // 1
    let userInfo = response.notification.request.content.userInfo
    
    guard let aps = userInfo["aps"] as? [String: AnyObject] else {
      return
    }
    
    guard let alert = aps["alert"] as? [String: AnyObject] else { return }
    
    (UIApplication.shared.windows.first?.rootViewController as? UITabBarController)?.selectedIndex = 1
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushNotification"), object: self, userInfo: alert)
    
    if let murl = alert["body"] as? String {
      if response.actionIdentifier == Identifiers.viewAction,
        let url = URL(string: murl) {
        let safari = SFSafariViewController(url: url)
        UIApplication.shared.windows.first?.rootViewController?.present(safari, animated: true)
      }
    }
    
  }
}



