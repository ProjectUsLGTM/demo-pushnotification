//
//  ViewController.swift
//  SharingPushNotification
//
//  Created by Adinan Yujaroen on 25/2/2563 BE.
//  Copyright © 2563 Adinan Yujaroen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet private weak var tableView: UITableView!
  var response: [MyTableCellViewModel] = []
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    // Do any additional setup after loading the view.
    
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(ViewController.receiveRefreshFeedNoti(_:)),
      name: Notification.Name(rawValue: "pushNotification"),
      object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  @objc func receiveRefreshFeedNoti(_ notification: Notification) {
    do {
      let data = try JSONSerialization.data(withJSONObject: notification.userInfo as Any, options: [])
      let response = try JSONDecoder().decode(MyTableCellViewModel.self, from: data)
      self.response.append(response)
    } catch {
      print(error)
    }

    DispatchQueue.main.async {
       self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
     }
  }
  
}

extension ViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.response.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell =  tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell") as? MyTableViewCell else { return UITableViewCell() }
    let model = self.response[indexPath.row]
    cell.updateUI(model: model)
    return cell
  }
}

extension ViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let alert = UIAlertController(title: response[indexPath.row].title, message: "url of this = \(response[indexPath.row].body)", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
      self.dismiss(animated: true, completion: nil)
    }))
    present(alert,animated: true)
  }
}

class MyTableViewCell: UITableViewCell {
  
  @IBOutlet private weak var headerLabel: UILabel!
  @IBOutlet private weak var messageLabel: UILabel!
  
  func updateUI(model: MyTableCellViewModel) {
    headerLabel.text = model.title
    messageLabel.text = model.subtitle
  }
}

public struct MyTableCellViewModel: Codable {
  let title: String
  let body: String
  let subtitle: String
  
  public init(title: String, body: String, subtitle: String) {
    self.title = title
    self.body = body
    self.subtitle = subtitle
  }
}
